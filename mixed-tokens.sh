 #!/bin/bash
echo mixed-tokens A
racket preprocess.rkt mixed-tokens/A.in > mixed-tokens/A.myout
diff --strip-trailing-cr mixed-tokens/A.out mixed-tokens/A.myout

echo mixed-tokens B
racket preprocess.rkt mixed-tokens/B.in > mixed-tokens/B.myout
diff --strip-trailing-cr mixed-tokens/B.out mixed-tokens/B.myout

echo mixed-tokens C
racket preprocess.rkt mixed-tokens/C.in > mixed-tokens/C.myout
diff --strip-trailing-cr mixed-tokens/C.out mixed-tokens/C.myout

echo mixed-tokens D
racket preprocess.rkt mixed-tokens/D.in > mixed-tokens/D.myout
diff --strip-trailing-cr mixed-tokens/D.out mixed-tokens/D.myout

echo mixed-tokens E
racket preprocess.rkt mixed-tokens/E.in > mixed-tokens/E.myout
diff --strip-trailing-cr mixed-tokens/E.out mixed-tokens/E.myout

echo mixed-tokens F
racket preprocess.rkt mixed-tokens/F.in > mixed-tokens/F.myout
diff --strip-trailing-cr mixed-tokens/F.out mixed-tokens/F.myout