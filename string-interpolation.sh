 #!/bin/bash
echo string-interpolation A
racket preprocess.rkt string-interpolation/A.in > string-interpolation/A.myout
diff --strip-trailing-cr string-interpolation/A.out string-interpolation/A.myout

echo string-interpolation B
racket preprocess.rkt string-interpolation/B.in > string-interpolation/B.myout
diff --strip-trailing-cr string-interpolation/B.out string-interpolation/B.myout

echo string-interpolation C
racket preprocess.rkt string-interpolation/C.in > string-interpolation/C.myout
diff --strip-trailing-cr string-interpolation/C.out string-interpolation/C.myout

echo string-interpolation D
racket preprocess.rkt string-interpolation/D.in > string-interpolation/D.myout
diff --strip-trailing-cr string-interpolation/D.out string-interpolation/D.myout