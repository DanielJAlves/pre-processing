 #!/bin/bash
echo type-alias A
racket preprocess.rkt type-alias/A.in > type-alias/A.myout
diff --strip-trailing-cr type-alias/A.out type-alias/A.myout

echo type-alias B
racket preprocess.rkt type-alias/B.in > type-alias/B.myout
diff --strip-trailing-cr type-alias/B.out type-alias/B.myout

echo type-alias C
racket preprocess.rkt type-alias/C.in > type-alias/C.myout
diff --strip-trailing-cr type-alias/C.out type-alias/C.myout

echo type-alias D
racket preprocess.rkt type-alias/D.in > type-alias/D.myout
diff --strip-trailing-cr type-alias/D.out type-alias/D.myout

echo type-alias E
racket preprocess.rkt type-alias/E.in > type-alias/E.myout
diff --strip-trailing-cr type-alias/E.out type-alias/E.myout

echo type-alias F
racket preprocess.rkt type-alias/F.in > type-alias/F.myout
diff --strip-trailing-cr type-alias/F.out type-alias/F.myout

echo type-alias G
racket preprocess.rkt type-alias/G.in > type-alias/G.myout
diff --strip-trailing-cr type-alias/G.out type-alias/G.myout