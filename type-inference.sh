 #!/bin/bash
echo type-inference A
racket preprocess.rkt type-inference/A.in > type-inference/A.myout
diff --strip-trailing-cr type-inference/A.out type-inference/A.myout

echo type-inference B
racket preprocess.rkt type-inference/B.in > type-inference/B.myout
diff --strip-trailing-cr type-inference/B.out type-inference/B.myout

echo type-inference C
racket preprocess.rkt type-inference/C.in > type-inference/C.myout
diff --strip-trailing-cr type-inference/C.out type-inference/C.myout

echo type-inference D
racket preprocess.rkt type-inference/D.in > type-inference/D.myout
diff --strip-trailing-cr type-inference/D.out type-inference/D.myout

echo type-inference E
racket preprocess.rkt type-inference/E.in > type-inference/E.myout
diff --strip-trailing-cr type-inference/E.out type-inference/E.myout

echo type-inference F
racket preprocess.rkt type-inference/F.in > type-inference/F.myout
diff --strip-trailing-cr type-inference/F.out type-inference/F.myout